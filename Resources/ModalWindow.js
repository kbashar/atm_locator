
var modalWin = function(dict)	{
	var popupWin = Titanium.UI.createWindow({
		backgroundColor: '#000000',
		opacity: 0.6,
		modal: true,
		navBarHidden:true,
		id: 'popupWin'
	});
	var popupView = Titanium.UI.createView({
		width: 200,
		height: 100,
		backgroundColor: 'white',
		borderRadius: 5,
		bordarColor: 'red',
		bordarWidth: 3
		});
	popupView.add(
		Ti.UI.createLabel({
			width: 150,
			height: 'auto',
			top: 10,
			text: (dict.address) ? dict.address: 'This is Modal Window',
			font: {fontSize: 14,fontWeight: 'bold'}
		})
	);
	popupWin.add(popupView);	
	popupWin.addEventListener('click',function(e){
		if(e.source.id != null)	{
			popupWin.close();
		}
	});
return popupWin;
};

module.exports.ModalWindow = modalWin;
