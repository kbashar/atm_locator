var NavigationController = require('NavigationController').NavigationController;
var SearchWindow = require('SearchWindow').SearchWindow;

var controller = new NavigationController();

var P = function (densityPixels) {
    return densityPixels*Ti.Platform.displayCaps.dpi/160;
};
 
var iconNums = 1;
var newIcon = function(dict) {
    var v = Ti.UI.createView({
        width:P(95),
        height:P(95),
        backgroundColor: (dict.color)?color: "#d85a1a"
    });
    v.add(Ti.UI.createLabel({
        text: (dict.bankName)? dict.bankName : iconNums++
    }));
    return v;
};
 
var dataR = [];
 
var newicon1 = new newIcon({bankName:'DBBL'});
	newicon1.addEventListener('click',function(){
		var searchWindow = new SearchWindow('DBBL',controller);
		controller.open(searchWindow);
	});
	dataR.push(newicon1);
	
var newicon2 = new newIcon({bankName:'AB Bank'});
	newicon2.addEventListener('click',function(){
		var searchWindow = new SearchWindow('AB Bank',controller);
		controller.open(searchWindow);
	});
	dataR.push(newicon2);
	
var newicon3 = new newIcon({bankName:'BRAC'});
	newicon3.addEventListener('click',function(){
		var searchWindow = new SearchWindow('BRAC',controller);
		controller.open(searchWindow);
	});
	dataR.push(newicon3);

var newicon4 = new newIcon({bankName:'CITY Bank'});
	newicon4.addEventListener('click',function(){
		var searchWindow = new SearchWindow('CITY Bank',controller);
		controller.open(searchWindow);
	});
	dataR.push(newicon4);

var win = Ti.UI.createWindow({
    backgroundColor:"black",
    navBarHidden:false,
    title:"Main Window"
});

var dashboard = require('dashboard'); 
win.add(dashboard.createDashboard({
    data: dataR
}));
 
controller.open(win);
