var createScrollableGridView = function (params) {
    var _p = function (densityPixels) {
        return densityPixels*Ti.Platform.displayCaps.dpi/160;
    };
var view = Ti.UI.createScrollView({
        scrollType: "vertical",
        cellWidth: (params.cellWidth)?params.cellWidth: _p(95),
        cellHeight: (params.cellHeight)?params.cellHeight: _p(95),
        xSpacer: (params.cellWidth)?params.cellWidth: _p(10),
        ySpacer: (params.cellHeight)?params.cellHeight: _p(10),
        xGrid: (params.xGrid)?params.xGrid:2,
        data: params.data
    });
 
    var objSetIndex = 0;
    var yGrid = view.data.length/view.xGrid;
 	Titanium.API.info("yGrid----------->"+yGrid); 
    for (var y=1; y<=yGrid; y++){
        var row = Ti.UI.createView({
            layout: "horizontal",
            focusable: false,
            top: y*(view.cellHeight+(2*view.ySpacer)),
            height: view.cellHeight+(2*view.ySpacer)
        });        
 
        for (var x=0; x<view.xGrid; x++){
            if(view.data[objSetIndex]){
                var thisView = Ti.UI.createView({
                    left: view.ySpacer,
                    height: view.cellHeight,
                    width: view.cellWidth
                });
                thisView.add(view.data[objSetIndex]);
                row.add(thisView);
                objSetIndex++;
           }
        }
        view.add(row);
    }
 
    return view;
};
module.exports.createDashboard = createScrollableGridView;