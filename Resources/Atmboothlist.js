var atmBoothList = function(dict,controller) {
	var win = Titanium.UI.createWindow({
		title : (dict.area_name) ? dict.area_name : "ATM Booth List",
		backgroundColor : '#fff',
	});

	var txtAutoComplete = Titanium.UI.createTextField({
		borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		editable : true,
		hintText : 'Type to Search',
		color : '#336699',
		top : 0,
		left : 10,
		width : 'auto',
		height : 60
	});

	//Table view showing your autocomplete values
	var tableview = Ti.UI.createTableView({
		top : 70,
		bottom : 0,
		width : '100%',
		backgroundColor : '#EFEFEF',
		allowSelection : true,
		layout : 'vertical'
	});

	var populateTable = function() {
		var db = require('Database');
		var areas = db.GetAllATMBooths({
			bankName : dict.bankName,
			area_name : dict.area_name
		});
		CreateAutoCompleteList(areas);
	};

	//Starts auto complete
	txtAutoComplete.addEventListener('change', function(e) {
		var pattern = e.source.value;
		if (pattern === "") {
			populateTable();
		}
		var db = require('Database');
		var tempArray = db.SearchATMBooths({
			bankName : dict.bankName,
			area_name : dict.area_name,
			address : pattern
		});
		CreateAutoCompleteList(tempArray);
	});

	//You got the required value and you clicks the word
	tableview.addEventListener('click', function(e) {
		var modalWin = require('ModalWindow').ModalWindow({address: e.rowData.result.address});
		controller.open(modalWin);				
	});

	//setting the tableview values
	function CreateAutoCompleteList(searchResults) {
		var tableData = [];
		for (var index = 0,
		    len = searchResults.length; index < len; index++) {

			var lblSearchResult = Ti.UI.createLabel({
				top : 2,
				width : '40%',
				height : 34,
				left : '5%',
				font : {
					fontSize : 14
				},
				color : '#000000',
				text : searchResults[index].name
			});

			//Creating the table view row
			var row = Ti.UI.createTableViewRow({
				backgroundColor : 'transparent',
				focusable : true,
				height : 50,
				result : searchResults[index]
			});

			row.add(lblSearchResult);
			tableData.push(row);
		}
		tableview.setData(tableData);
	}

	populateTable();
	win.add(txtAutoComplete);
	win.add(tableview);
	return win;
};
module.exports.ATMBoothList = atmBoothList;
