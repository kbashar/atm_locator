exports.SearchWindow = function(bankName, controller) {
	// create base UI tab and root searchWindow
	var bank_name = (bankName) ? bankName : 'Bank Name';

	var searchWin = Titanium.UI.createWindow({
		title : bank_name,
		backgroundColor : '#fff',
	});

	var txtAutoComplete = Titanium.UI.createTextField({
		borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		editable : true,
		hintText : 'Type to Search',
		color : '#336699',
		top : 0,
		left : 10,
		width : 'auto',
		height : 60
	});

	//Table view showing your autocomplete values
	var tableview = Ti.UI.createTableView({
		top : 70,
		bottom : 0,
		width : '100%',
		backgroundColor : '#EFEFEF',
		allowSelection : true,
		layout : 'vertical'
	});

	var populateTable = function() {
		var db = require('Database');
		var areas = db.GetAllArea({
			bankName : bankName
		});
		CreateAutoCompleteList(areas);
	};

	//Starts auto complete
	txtAutoComplete.addEventListener('change', function(e) {
		var pattern = e.source.value;
		if (pattern === "") {
			return;
		}
		var db = require('Database');
		var tempArray = db.SearchForArea({
			bankName : bankName,
			area : pattern
		});
		CreateAutoCompleteList(tempArray);
	});

	//You got the required value and you clicks the word
	tableview.addEventListener('click', function(e) {
		var atmBoothListWindow = require('Atmboothlist').ATMBoothList({
			bankName : bankName,
			area_name : e.rowData.result
		}, controller);
		controller.open(atmBoothListWindow);
	});

	//setting the tableview values
	function CreateAutoCompleteList(searchResults) {
		var tableData = [];
		for (var index = 0,
		    len = searchResults.length; index < len; index++) {

			var lblSearchResult = Ti.UI.createLabel({
				top : 2,
				width : '40%',
				height : 34,
				left : '5%',
				font : {
					fontSize : 14
				},
				color : '#000000',
				text : searchResults[index]
			});

			//Creating the table view row
			var row = Ti.UI.createTableViewRow({
				backgroundColor : 'transparent',
				focusable : true,
				height : 50,
				result : searchResults[index]
			});

			row.add(lblSearchResult);
			tableData.push(row);
		}
		tableview.setData(tableData);
	}

	populateTable();
	searchWin.add(txtAutoComplete);
	searchWin.add(tableview);

	searchWin.open();

	return searchWin;
};
