var searchForArea = function(dict)	{
	var db = Titanium.Database.install('atmlocator.db','atmlocator');
	var data = [];
	if (!dict.area)	{
		return data;
	} else {
		var resultSet = db.execute('Select DISTINCT atm_area from '+dict.bankName +' where atm_area like \''+dict.area+'%\'');
		while(resultSet.isValidRow())	{
			var area_name = resultSet.fieldByName('atm_area');
			Ti.API.info(area_name);
			data.push(area_name);
			resultSet.next();
		}
		resultSet.close();
		db.close();
		return data;
	}
};

 var searchForATMBooths = function(dict)	{
 	var db = Titanium.Database.install('atmlocator.db','atmlocator');
	var data = [];
	if (!dict.selectedArea)	{
		return data;
	} else {
		var resultSet = db.execute('Select atm_name,atm_address,atm_area,atm_district from '+dict.bankName +' where atm_area = \''+dict.selectedArea+'\' COLLATE NOCASE');
		while(resultSet.isValidRow())	{
			Ti.API.info(
				resultSet.fieldByName('atm_name')+
				"...."+
				resultSet.fieldByName('atm_address')+
				"...."+
				resultSet.fieldByName('atm_area'));
			resultSet.next();
		}
		resultSet.close();
		db.close();
		return data;
	}	
};

var getAllArea = function(dict) {
	var db = Titanium.Database.install('atmlocator.db','atmlocator');
	var data = [];
		var resultSet = db.execute('Select DISTINCT atm_area from '+dict.bankName);
		while(resultSet.isValidRow())	{
			var area_name = resultSet.fieldByName('atm_area');
			Ti.API.info(area_name);
			data.push(area_name);
			resultSet.next();
		}
		resultSet.close();
		db.close();
		return data;
};

var getAllATMBooths = function(dict)	{
	var db = Ti.Database.open('atmlocator');
	var data = [];
	
	var resultSet = db.execute('select * from ' + dict.bankName + ' where atm_area = \'' + dict.area_name + '\' COLLATE NOCASE');
	while (resultSet.isValidRow()) {
		data.push({
			name : resultSet.fieldByName('atm_name'),
			address: resultSet.fieldByName('atm_address')
		});
		resultSet.next();
	}
	resultSet.close();
	db.close();
	return data;
};

var searchATMBooths = function(dict)	{
	var db = Ti.Database.open('atmlocator');
	var data = [];
	Titanium.API.info(dict.bankName +"....."+dict.area_name+"......"+dict.address);
	Titanium.API.info('select * from ' + dict.bankName + ' where atm_area = \'' + dict.area_name + '\' and atm_address like \'%'+dict.address+'%\' COLLATE NOCASE');
	var resultSet = db.execute('select * from ' + dict.bankName + ' where atm_area = \'' + dict.area_name + '\' and atm_address like \'%'+dict.address+'%\' COLLATE NOCASE');
	while (resultSet.isValidRow()) {
		data.push({
			name : resultSet.fieldByName('atm_name'),
			address: resultSet.fieldByName('atm_address')
		});
		resultSet.next();
	}
	resultSet.close();
	db.close();
	return data;
};


module.exports.SearchForArea = searchForArea;
module.exports.SearchForATMBooths = searchForATMBooths;
module.exports.GetAllArea = getAllArea;
module.exports.GetAllATMBooths = getAllATMBooths;
module.exports.SearchATMBooths = searchATMBooths;